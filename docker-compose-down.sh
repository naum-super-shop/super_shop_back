#! /bin/bash

sudo chown -R $USER:$USER *

cp ./_CI/docker-compose.yml ./

export NGINX_PORT=80

docker-compose -p super_shop_local down
docker system prune -f

rm -f ./docker-compose.yml

rm -rf ./web/logs/ ./web/media/ ./web/static/