#! /bin/bash

sudo chown -R $USER:$USER *

rm -f ./web/web/.env
echo "DEBUG=on" >> ./web/web/.env
echo "POSTGRES_PORT=5432" >> ./web/web/.env
echo "POSTGRES_HOST=postgres" >> ./web/web/.env
echo "POSTGRES_DB=super_shop" >> ./web/web/.env
echo "POSTGRES_USER=super_shop" >> ./web/web/.env
echo "POSTGRES_PASSWORD=super_shop" >> ./web/web/.env
echo "SECRET_KEY=%tnuad3h32-4c#gdljwh%9)8d++n-weu^(587-c!o5at*4an9r" >> ./web/web/.env
echo "DATABASE_URL=psql://super_shop:super_shop@postgres:5432/super_shop" >> ./web/web/.env

cp ./_CI/docker-compose.yml ./

export NGINX_PORT=80

docker-compose -p super_shop_local down
docker-compose -p super_shop_local build
docker-compose -p super_shop_local up -d

docker system prune -f

rm -f ./docker-compose.yml

rm -rf ./web/logs/ ./web/media/ ./web/static/