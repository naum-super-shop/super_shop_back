from django.core.management import call_command
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = 'Заполнение БД'
    
    def handle(self, *args, **options):
        self.stdout.write('Populating Data Base...')
        
        call_command('seed_custom_user_model')
        
        self.stdout.write('Done.')
