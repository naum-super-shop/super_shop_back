from drf_yasg import openapi
from django.conf import settings
from django.contrib import admin
from rest_framework import routers
from django.urls import path, include
from drf_yasg.views import get_schema_view
from django.conf.urls.static import static
from rest_framework.permissions import AllowAny
from rest_framework_swagger.views import get_swagger_view

router = routers.DefaultRouter()

schema_view = get_schema_view(
    openapi.Info(
        title="Super Shop API",
        default_version='v1',
    ),
    public=True,
    permission_classes=(AllowAny,),
)

urlpatterns = [
    path('admin/', admin.site.urls),
    
    # path('api/', include(router.urls)),
    path('api/swagger/', get_swagger_view(title='Super Shop API')),
    path('api/docs/', schema_view.with_ui('redoc', cache_timeout=None), name='schema-redoc'),
    
    path('api/user/', include('apps.users.urls', namespace='user')),
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
