from django.urls import path
from rest_framework.routers import DefaultRouter

from apps.users.views import CustomUserLoginView, UserProfileViewset

# router = DefaultRouter()

app_name = 'user2'

urlpatterns = [
    path('login/', CustomUserLoginView.as_view(), name='login'),
    path('profile/', UserProfileViewset.as_view({'get': 'retrieve', 'put': 'update'}), name='profile'),
]
