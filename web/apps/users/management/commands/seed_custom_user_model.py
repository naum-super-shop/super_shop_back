import random
from datetime import datetime

from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand
from transliterate import translit

from apps.users.models import UserProfile


class Command(BaseCommand):
    help = 'Заполнение моделей пользователя и его профиля'
    
    def handle(self, *args, **options):
        self.stdout.write('Seed CustomUser model...')
        User = get_user_model()
        
        last_names = ("Скворцов", "Кузнецов", "Сорокин", "Петров", "Иванов")
        first_names = ("Илья", "Вадим", "Андрей", "Руслан", "Роман", "Константин", "Иван")
        second_names = ("Викторович", "Романович", "Петрович", "Александрович", "Андреевич")
        birthdays = ("12.12.2012", "09.08.2000", "02.09.1990", "15.01.1960", "07.07.1977", "22.02.2002")
        phones = ("8-921-232-23-90", "8-921-242-63-93", "8-945-342-34-34", "8-956-287-45-67", "8-952-673-98-56")
        
        for index, last_name in enumerate(last_names):
            first_name = random.choice(first_names)
            second_name = random.choice(second_names)
            birthday = datetime.strptime(random.choice(birthdays), '%d.%m.%Y').date()
            email = f'{translit(last_name.lower(), "ru", reversed=True)}@user.com'
            
            if not User.objects.filter(email=email):
                user = User.objects.create_user(email=email, password='qwerty123')
                user.update_profile(last_name=last_name, first_name=first_name, second_name=second_name,
                                    birthday=birthday, phone=phones[index])
        
        if not User.objects.filter(email='admin@admin.com'):
            User.objects.create_superuser(email='admin@admin.com', password='adminAdmin')
        
        self.stdout.write('Seeding of model finished.')
