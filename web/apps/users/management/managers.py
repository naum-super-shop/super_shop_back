from django.contrib.auth.base_user import BaseUserManager


class CustomUserManager(BaseUserManager):
    """ Кастомный менеджер модели пользователя """
    
    def create_user(self, email: str, password: str, **extra_fields) -> object:
        """ Создание простого пользователя """
        
        if not email:
            raise ValueError('Поле e-mail не может быть пустым.')
        
        email = self.normalize_email(email)
        user = self.model.objects.create(email=email, **extra_fields)
        user.set_password(password)
        user.create_profile()
        user.save()
        return user
    
    def create_superuser(self, email: str, password: str, **extra_fields) -> object:
        """ Создание суперпользователя """
        
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_active', True)
        extra_fields.setdefault('is_superuser', True)
        
        if extra_fields.get('is_staff') is not True:
            raise ValueError('Суперпользователь должен иметь is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Суперпользователь должен иметь is_superuser=True.')
        
        return self.create_user(email, password, **extra_fields)
