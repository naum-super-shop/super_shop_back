from django.utils.timezone import now
from rest_framework import serializers

from apps.users.models import CustomUser, UserProfile


class LoginSerializer(serializers.Serializer):
    """ Сериалайзер для авторизации """
    
    email = serializers.EmailField(required=True)
    password = serializers.CharField(required=True)
    
    class Meta:
        model = CustomUser
        fields = ('email', 'password')
    
    def validate(self, attrs):
        email = attrs.get('email')
        password = attrs.get('password')
        user = CustomUser.objects.filter(email=email)
        
        if not user:
            raise serializers.ValidationError({'email': 'Пользователя с таким e-mail не существует'})
        
        if not user.first().check_password(password):
            raise serializers.ValidationError({'password': 'Неверный пароль'})
        
        return attrs


class CustomUserSerializer(serializers.ModelSerializer):
    """ Сериалайзер для кастомной модели пользователя """
    
    class Meta:
        model = CustomUser
        fields = ('id', 'email', 'is_staff')


class UserProfileSerializer(serializers.ModelSerializer):
    """ Сериалайзер для профиля пользователя """
    
    user = CustomUserSerializer(read_only=True)
    
    class Meta:
        model = UserProfile
        fields = '__all__'
    
    def validate(self, attrs):
        birthday = attrs['birthday'] if 'birthday' in attrs else None
        
        if birthday and birthday > now().today().date():
            raise serializers.ValidationError('Дата рождения не может быть в будущем')
        
        return attrs
