from datetime import date

from django.db import models
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.base_user import AbstractBaseUser

from apps.users.management.managers import CustomUserManager


class CustomUser(AbstractBaseUser, PermissionsMixin):
    """ Кастомная модель пользователя """
    
    email = models.EmailField(verbose_name='E-mail', max_length=100, unique=True)
    is_staff = models.BooleanField(verbose_name='Статус персонала', default=False)
    is_active = models.BooleanField(verbose_name='Активный', default=True)
    created_at = models.DateTimeField(verbose_name='Дата регистрации', auto_now_add=True)
    updated_at = models.DateTimeField(verbose_name='Дата измененияя', auto_now=True)
    
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []
    
    objects = CustomUserManager()
    
    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'
        ordering = ('-created_at',)
    
    def __str__(self) -> str:
        return self.email
    
    def create_profile(self, **kwargs):
        UserProfile.objects.create(user=self, **kwargs)
    
    def update_profile(self, **kwargs):
        UserProfile.objects.filter(user=self).update(**kwargs)


class UserProfile(models.Model):
    """ Модель профиль пользователя """
    
    user = models.OneToOneField(CustomUser, verbose_name='Пользоваетель', related_name='profile',
                                on_delete=models.CASCADE, primary_key=True)
    last_name = models.CharField(verbose_name='Фамилия', max_length=100)
    first_name = models.CharField(verbose_name='Имя', max_length=100)
    second_name = models.CharField(verbose_name='Отчество', max_length=100, blank=True, null=True)
    birthday = models.DateField(verbose_name='Дата рождения', blank=True, null=True)
    phone = models.CharField(verbose_name='Телефон', max_length=100, blank=True, null=True)
    
    class Meta:
        verbose_name = 'Профиль'
        verbose_name_plural = 'Профили'
        ordering = ('user__email',)
    
    def __str__(self):
        return f'{self.last_name} {self.first_name[:1]}.' \
            if len(self.last_name) and len(self.first_name) else self.user.email
    
    def get_full_name(self) -> str:
        """ Получить полное имя пользователя """
        full_name = f'{self.last_name} {self.first_name} {self.second_name}'
        return full_name.strip()
    
    def get_last_and_first_name(self) -> str:
        """ Получить фамилию и имя """
        return f'{self.last_name} {self.first_name}'
    
    def get_age(self) -> int:
        """ Получить возраст """
        today = date.today()
        age = today.year - self.birthday.year - ((today.month, today.day) < (self.birthday.month, self.birthday.day))
        return age
    
    def get_suffix_age(self) -> str:
        """ Получить возраст c суффиксом """
        age = self.get_age()
        mod = age % 10
        
        if 11 <= age <= 14 or mod >= 5 or mod == 0:
            return f'{age} лет'
        elif 2 <= mod <= 4:
            return f'{age} года'
        elif mod == 1:
            return f'{age} год'
