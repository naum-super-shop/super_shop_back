from datetime import timedelta

from django.utils.timezone import now
from rest_framework import status
from rest_framework.test import APITestCase

from apps.users.models import CustomUser


class UserProfileViewsetTest(APITestCase):
    
    def setUp(self) -> None:
        test_user = CustomUser.objects.create(email='test@user.com', password='qwerty')
        test_user.create_profile(last_name='Иванов', first_name='Иван', second_name='Иванович', birthday='2000-01-01')
    
    def test_get_profile(self):
        resp = self.client.get('/api/user/profile/')
        self.assertEqual(resp.status_code, status.HTTP_401_UNAUTHORIZED)
        
        user = CustomUser.objects.get(email='test@user.com')
        self.client.force_authenticate(user=user)
        resp = self.client.get('/api/user/profile/')
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
    
    def test_put_profile(self):
        resp = self.client.put('/api/user/profile/', {'last_name': 'Иванов', 'first_name': 'Иван'})
        self.assertEqual(resp.status_code, status.HTTP_401_UNAUTHORIZED)
        
        user = CustomUser.objects.get(email='test@user.com')
        self.client.force_authenticate(user=user)
        
        resp = self.client.put('/api/user/profile/', {'last_name': 'Иванов', 'first_name': ''})
        self.assertEqual(resp.status_code, status.HTTP_400_BAD_REQUEST)
        
        resp = self.client.put('/api/user/profile/', {'last_name': '', 'first_name': 'Иван'})
        self.assertEqual(resp.status_code, status.HTTP_400_BAD_REQUEST)
        
        resp = self.client.put('/api/user/profile/', {'last_name': 'Иванов', 'first_name': 'Иван'})
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        
        resp = self.client.put('/api/user/profile/',
                               {
                                   'last_name': 'Иванов',
                                   'first_name': 'Иван',
                                   'birthday': (now() + timedelta(days=30)).date()
                               })
        self.assertEqual(resp.status_code, status.HTTP_400_BAD_REQUEST)
