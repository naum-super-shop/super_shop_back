from django.test import TestCase
from django.contrib.auth import get_user_model


class UsersManagersTests(TestCase):
    """ Тестирование менеджера пользователй """
    
    def test_create_user(self) -> None:
        User = get_user_model()
        user = User.objects.create_user(email='simpl@user.com', password='qwerty')
        
        self.assertEqual(user.email, 'simpl@user.com')
        self.assertTrue(user.is_active)
        self.assertFalse(user.is_staff)
        self.assertFalse(user.is_superuser)
        
        try:
            self.assertIsNone(user.username)
        except AttributeError:
            pass
        
        with self.assertRaises(TypeError):
            User.objects.create_user()
        with self.assertRaises(TypeError):
            User.objects.create_user(email='')
        with self.assertRaises(ValueError):
            User.objects.create_user(email='', password='qwerty')
    
    def test_create_superuser(self) -> None:
        User = get_user_model()
        superuser = User.objects.create_superuser(email='super@user.com', password='qwerty')
        
        self.assertEqual(superuser.email, 'super@user.com')
        self.assertTrue(superuser.is_staff)
        self.assertTrue(superuser.is_active)
        self.assertTrue(superuser.is_superuser)
        
        try:
            self.assertIsNone(superuser.username)
        except AttributeError:
            pass
        
        with self.assertRaises(ValueError):
            User.objects.create_superuser(email='super@user.com', password='qwerty', is_superuser=False)
