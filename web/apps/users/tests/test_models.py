from datetime import timedelta

from django.test import TestCase
from django.utils.timezone import now

from apps.users.models import CustomUser, UserProfile


class CustomUserModelTest(TestCase):
    """ Тестирование кастомной модели пользователя """
    
    def setUp(self) -> None:
        CustomUser.objects.create(email='test@user.com', password='testTest')
    
    def test_custom_user_str(self):
        user = CustomUser.objects.get(email='test@user.com')
        self.assertEqual(str(user), 'test@user.com')
    
    def test_create_profile(self):
        user = CustomUser.objects.get(email='test@user.com')
        user.create_profile(last_name='Иванов', first_name='Иван')
        
        user_profile = UserProfile.objects.get(user=user)
        self.assertEqual(str(user_profile), 'Иванов И.')
    
    def test_update_profile(self):
        user = CustomUser.objects.get(email='test@user.com')
        user.create_profile(last_name='Иванов', first_name='Иван')
        user.update_profile(last_name='Петров', first_name='Петр')
        
        user_profile = UserProfile.objects.get(user=user)
        self.assertEqual(str(user_profile), 'Петров П.')


class UserProfileTest(TestCase):
    """ Тестирование модели профиля пользователя """
    
    def setUp(self) -> None:
        age_11 = (now() - timedelta(days=365 * 11 + 33)).date()
        age_21 = (now() - timedelta(days=365 * 22 - 33)).date()
        age_22 = (now() - timedelta(days=365 * 22 + 33)).date()
        age_26 = (now() - timedelta(days=365 * 26 + 33)).date()
        
        user_age_11 = CustomUser.objects.create(email='age_11_test@user.com')
        user_age_21 = CustomUser.objects.create(email='age_21_test@user.com')
        user_age_22 = CustomUser.objects.create(email='age_22_test@user.com')
        user_age_26 = CustomUser.objects.create(email='age_26_test@user.com')
        test_user = CustomUser.objects.create(email='test@user.com')
        
        user_age_11.create_profile(birthday=age_11)
        user_age_21.create_profile(birthday=age_21)
        user_age_22.create_profile(birthday=age_22)
        user_age_26.create_profile(birthday=age_26)
        test_user.create_profile(last_name='Иванов', first_name='Иван', second_name='Иванович')
    
    def test_custom_user_str(self):
        user = UserProfile.objects.get(user__email='test@user.com')
        user_age_11 = UserProfile.objects.get(user__email='age_11_test@user.com')
        
        self.assertEqual(str(user), 'Иванов И.')
        self.assertEqual(str(user_age_11), 'age_11_test@user.com')
    
    def test_get_full_name(self):
        user = UserProfile.objects.get(user__email='test@user.com')
        self.assertEqual(user.get_full_name(), 'Иванов Иван Иванович')
    
    def test_get_last_and_first_name(self):
        user = UserProfile.objects.get(user__email='test@user.com')
        self.assertEqual(user.get_last_and_first_name(), 'Иванов Иван')
    
    def test_get_age(self):
        user_age_21 = UserProfile.objects.get(user__email='age_21_test@user.com')
        user_age_22 = UserProfile.objects.get(user__email='age_22_test@user.com')
        
        self.assertEqual(user_age_21.get_age(), 21)
        self.assertEqual(user_age_22.get_age(), 22)
    
    def test_get_human_age(self):
        user_age_11 = UserProfile.objects.get(user__email='age_11_test@user.com')
        user_age_21 = UserProfile.objects.get(user__email='age_21_test@user.com')
        user_age_22 = UserProfile.objects.get(user__email='age_22_test@user.com')
        user_age_26 = UserProfile.objects.get(user__email='age_26_test@user.com')
        
        self.assertEqual(user_age_11.get_suffix_age(), '11 лет')
        self.assertEqual(user_age_21.get_suffix_age(), '21 год')
        self.assertEqual(user_age_22.get_suffix_age(), '22 года')
        self.assertEqual(user_age_26.get_suffix_age(), '26 лет')
