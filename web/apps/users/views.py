from knox.models import AuthToken
from rest_framework import status
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.viewsets import generics
from rest_framework.permissions import AllowAny, IsAuthenticated

from apps.users.models import CustomUser, UserProfile
from apps.users.serializers import LoginSerializer, CustomUserSerializer, UserProfileSerializer


class CustomUserLoginView(generics.CreateAPIView):
    """ Авторизация пользователя """
    
    queryset = CustomUser.objects.all()
    serializer_class = LoginSerializer
    permission_classes = (AllowAny,)
    
    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        is_valid = serializer.is_valid()
        
        if is_valid:
            user = CustomUser.objects.get(email=serializer.data.get('email'))
            data = {
                'user': CustomUserSerializer(user).data,
                'token': AuthToken.objects.create(user=user)[1],
            }
            
            return Response(data=data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class UserProfileViewset(viewsets.ModelViewSet):
    """ Профиль пользователя """
    
    queryset = UserProfile.objects.all()
    serializer_class = UserProfileSerializer
    permission_classes = (IsAuthenticated,)
    
    def retrieve(self, request, *args, **kwargs):
        user = self.request.user
        serializer = self.get_serializer(user.profile)
        return Response(serializer.data, status=status.HTTP_200_OK)
    
    def update(self, request, *args, **kwargs):
        user = self.request.user
        serializer = self.get_serializer(instance=user.profile, data=request.data)
        is_valid = serializer.is_valid()
        
        if is_valid:
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
