#! /bin/bash

# Ожидаем запуска БД
while ! curl http://postgres:5432/ 2>&1 | grep '52'; do sleep 1; done

python manage.py migrate --no-input
python manage.py collectstatic --no-input
python manage.py seed_db

gunicorn --bind 0.0.0.0:8000 web.wsgi --workers=3